﻿//using GEUTEBRUECK.GeViScope.Wrapper.DBI;
//using GEUTEBRUECK.GeViScope.Wrapper.MediaPlayer;
using GEUTEBRUECK.Gng.Wrapper.DBI;
using GEUTEBRUECK.Gng.Wrapper.MediaPlayer;
using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace GCoreLibrary
{
    public partial class GCoreForm : Form
    {
        //private GscPLCWrapper _gscPLCWrapper;
        private GngPLCWrapper _gngPLCWrapper;
        //private GscServer _gscServer;
        private GngServer _gngServer;
        //private GscViewer _gscViewer;
        private GngViewer _gngViewer;
        private string _message;
        private bool _showingMessage;
        private Bitmap _snapshotImage;
        private GCoreViewerForm _gCoreViewerForm;
        private string _username;

        private enum STATUS
        {
            UNKNOWN,
            OK,
            NO_SERVER_CONNECTION,
            NO_VIDEO_CONNECTION
        }

        private STATUS CaptureImage(out Bitmap image, string comments)
        {
            if (_gngViewer == null || _gngServer == null || !_gngServer.IsConnected)
            {
                image = null;
                return STATUS.NO_SERVER_CONNECTION;
            }

            //GscMPPictureExportParams exportParams = new GscMPPictureExportParams();
            GngMPPictureExportParams exportParams = new GngMPPictureExportParams();
            //exportParams.DestType = GscExportDestType.edtBMP;
            exportParams.DestType = GngExportDestType.edtBMP;
            exportParams.FileName = System.IO.Path.GetTempFileName();
            exportParams.FileName = exportParams.FileName.Replace(".tmp", ".bmp");
            exportParams.DontShowDialog = true;

            try
            {
                _gngViewer.ExportSinglePicture(exportParams);
            }
            //catch (GscGMPException)
            catch (GngGMPException)
            {
                image = null;
                return STATUS.NO_VIDEO_CONNECTION;
            }

            Bitmap videoCapture = new Bitmap(exportParams.FileName);

            Graphics g = Graphics.FromImage(videoCapture);
            g.PageUnit = GraphicsUnit.Pixel;

            Font drawingFont = new Font(FontFamily.GenericMonospace, 10.0f);

            SizeF stringSize = g.MeasureString(comments, drawingFont);

            image = new Bitmap(videoCapture.Width, videoCapture.Height + (int)((stringSize.Height + 2) * drawingFont.SizeInPoints));

            g = Graphics.FromImage(image);

            g.Clear(Color.White);
            g.DrawImage(videoCapture, 0, 0, videoCapture.Width, videoCapture.Height);
            g.DrawString(comments, drawingFont, Brushes.Black, 0.0f, videoCapture.Height);

            return STATUS.OK;
        }

        private bool ChannelIsBlocked(long number)
        {
            if (_gngServer == null)
            {
                return true;
            }

            //GscRegistry gscRegistry = _gscServer.CreateRegistry();
            GngRegistry gscRegistry = _gngServer.CreateRegistry();
            if (gscRegistry != null)
            {
                //GscRegistryReadRequest[] readRequests = new GscRegistryReadRequest[1];
                GngRegistryReadRequest[] readRequests = new GngRegistryReadRequest[1];
                //readRequests[0] = new GscRegistryReadRequest("/", 0);
                readRequests[0] = new GngRegistryReadRequest("/", 0);

                gscRegistry.ReadNodes(readRequests);
            }

            gscRegistry.GetUserAccount(_username, out Guid userAccount);

            ArrayList mediaChannels = new ArrayList();
            gscRegistry.GetMediaChannels(out mediaChannels);

            foreach (Guid mediaChannel in mediaChannels)
            {
                gscRegistry.GetMediaChannelSettings(mediaChannel, out long mappedId, out long globalNumber, out bool active, out string name, out string description);

                if (number == globalNumber)
                {
                    gscRegistry.GetBlockedMediaChannelSettings(userAccount, mediaChannel, out bool blockAudio, out bool blockDatabase, out bool blockLive, out bool blockTelecontrol);
                    return blockLive;
                }
            }

            gscRegistry.Dispose();

            return false;
        }

        private bool ConnectToServer(string address, string username, string password)
        {
            bool connected = false;

            if (_gngServer == null)
            {
                //_gscServer = new GscServer();
                _gngServer = new GngServer();
            }

            string encodedPassword = DBIHelperFunctions.EncodePassword(password);

            //using (GscServerConnectParams connectParams = new GscServerConnectParams(address, username, encodedPassword))
            using (GngServerConnectParams connectParams = new GngServerConnectParams(address, username, encodedPassword))
            {
                _gngServer.SetConnectParams(connectParams);

                //GscServerConnectResult connectResult = _gscServer.Connect();
                GngServerConnectResult connectResult = _gngServer.Connect();

                //connected = (connectResult == GscServerConnectResult.connectOk);
                connected = (connectResult == GngServerConnectResult.connectOk);

                if (connected)
                {
                    _username = username;
                    CreatePLC();
                }
            }

            return connected;
        }

        private void CreatePLC()
        {
            DestroyPLC();

            _gngPLCWrapper = _gngServer.CreatePLC();

            _gngPLCWrapper.PLCCallback += new PLCCallbackEventHandler(PLCCallback);
            _gngPLCWrapper.OpenPushCallback();
        }

        private void CustomDrawCallback(CustomDrawCallbackEventArgs e)
        {
            if (_message != null)
            {
                e.ViewerDC.FillRectangle(new SolidBrush(Color.Red), ClientRectangle);
                e.ViewerDC.DrawString(_message, new Font("Arial", 16), Brushes.White, 0.0f, 0.0f);
                e.DoUpdateBackbuffer = true;
                _showingMessage = true;
            }
            else
            {
                if (_showingMessage)
                {
                    e.DoUpdateBackbuffer = true;
                    _showingMessage = false;
                }
                else
                {
                    e.DoUpdateBackbuffer = true;
                }
            }

            BeginInvoke(new CustomDrawCallbackDelegate(ResizePanelToVideoSize), new object[] { e });
        }

        private void ResizePanelToVideoSize(CustomDrawCallbackEventArgs e)
        {
            if ((WindowState != FormWindowState.Minimized) && e.ViewerStatus.IsConnected)
            {
                int leftMargin = pnlViewer.Left;
                int rightMargin = ClientRectangle.Right - pnlViewer.Right;
                int topMargin = pnlViewer.Top;
                int bottomMargin = ClientRectangle.Bottom - pnlViewer.Bottom;
                int width = e.SrcRect.Width + leftMargin + rightMargin + 2;
                int height = e.SrcRect.Height + topMargin + bottomMargin + 2;

                SetClientSizeCore(width, height);
            }
        }

        private void DestroyPLC(bool connectionLost = false)
        {
            if (_gngPLCWrapper != null)
            {
                if (!connectionLost)
                {
                    _gngPLCWrapper.UnsubscribeAll();
                    _gngPLCWrapper.CloseCallback();
                }

                _gngPLCWrapper.Dispose();
                _gngPLCWrapper = null;
            }
        }

        private void DisconnectFromServer(bool connectionLost = false)
        {
            if (_gngViewer != null)
            {
                _gngViewer.Disconnect(true);
            }

            DestroyPLC(connectionLost);

            if (_gngServer != null)
            {
                _gngServer.Disconnect(System.Threading.Timeout.Infinite);
                _gngServer.Dispose();
                _gngServer = null;
            }

            if (connectionLost)
            {
                Invoke((Action)(() =>
                {
                    OnBtnDisconnectClick(this, new EventArgs());
                    MessageBox.Show("Lost connection to video server.", "Video Server Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }));
            }
        }

        private void FillMediaChannelList(bool showAll)
        {
            lbMediaChannels.Items.Clear();

            bool success = GetMediaChannelList(out ArrayList mediaChannelList);

            if (success && mediaChannelList.Count > 0)
            {
                foreach (GCoreMediaChannelDescriptor mediaChannelDescriptor in mediaChannelList)
                {
                    if (!mediaChannelDescriptor.Blocked || showAll)
                    {
                        lbMediaChannels.Items.Add(mediaChannelDescriptor);
                    }
                }

                lbMediaChannels.DisplayMember = "Name";
                lbMediaChannels.SelectedIndex = 0;
            }
        }

        private bool GetMediaChannelList(out ArrayList mediaChannelList)
        {
            mediaChannelList = new ArrayList();

            if (_gngServer == null)
            {
                return false;
            }

            ArrayList mediaChannels = new ArrayList();
            MediaPlayerHelperFunctions.QueryMediaChannelList(_gngServer, out mediaChannels);
            //foreach (GscMediaChannelData mediaChannel in mediaChannels)
            foreach (GngMediaChannelData mediaChannel in mediaChannels)
            {
                GCoreMediaChannelDescriptor mediaChannelDescriptor = new GCoreMediaChannelDescriptor(mediaChannel.ChannelID, mediaChannel.GlobalNumber, mediaChannel.Name, mediaChannel.Desc, ChannelIsBlocked(mediaChannel.GlobalNumber));
                mediaChannelList.Add(mediaChannelDescriptor);
            }

            return true;
        }

        private STATUS GetStatus()
        {
            STATUS status = STATUS.UNKNOWN;

            if (_gngServer == null || !_gngServer.IsConnected)
            {
                status = STATUS.NO_SERVER_CONNECTION;
            }
            else if (_gngViewer != null)
            {
                //GscMPPictureExportParams exportParams = new GscMPPictureExportParams();
                GngMPPictureExportParams exportParams = new GngMPPictureExportParams();
                //exportParams.DestType = GscExportDestType.edtBMP;
                exportParams.DestType = GngExportDestType.edtBMP;
                exportParams.FileName = System.IO.Path.GetTempFileName();
                exportParams.FileName = exportParams.FileName.Replace(".tmp", ".bmp");
                exportParams.DontShowDialog = true;

                status = STATUS.OK;
                try
                {
                    _gngViewer.ExportSinglePicture(exportParams);
                }
                //catch (GscGMPException)
                catch (GngGMPException)
                {
                    status = STATUS.NO_VIDEO_CONNECTION;
                }
            }
            else
            {
                status = STATUS.NO_VIDEO_CONNECTION;
            }

            return status;
        }

        private void OnBtnConnectClick(object sender, EventArgs e)
        {
            string serverAddress = edtVideoServerAddress.Text.Trim();
            string[] usernameParts = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\');
            string username = usernameParts[usernameParts.Length - 1];
            string password = username;

            if (serverAddress != "")
            {
                bool connected = ConnectToServer(serverAddress, username, password);

                if (connected)
                {
                    btnConnect.Enabled = false;
                    edtVideoServerAddress.Enabled = false;
                    btnDisconnect.Enabled = true;

                    FillMediaChannelList(true);
                    SelectMediaChannel();
                }
                else
                {
                    MessageBox.Show(string.Format("Unable to connect to {0}.", serverAddress), "Video Server Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void OnBtnDisconnectClick(object sender, EventArgs e)
        {
            lbMediaChannels.Items.Clear();

            DisconnectFromServer();

            SetClientSizeCore(640, 480);
            pnlViewer.Refresh();

            btnConnect.Enabled = true;
            edtVideoServerAddress.Enabled = true;
            btnDisconnect.Enabled = false;
        }

        private void OnEdtVideoServerAddressKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OnBtnConnectClick(sender, new EventArgs());
            }
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void OnLbMediaChannelSelectedIndexChanged(object sender, EventArgs e)
        {
            SelectMediaChannel();
        }

        private void PLCCallback(object sender, PLCCallbackEventArgs e)
        {
            //if (e.PlcNotification.GetNotificationType() == GscPlcNotificationType.plcnPushCallbackLost)
            if (e.PlcNotification.GetNotificationType() == GngPlcNotificationType.plcnPushCallbackLost)
            {
                DisconnectFromServer(true);
            }
        }

        private void SelectMediaChannel()
        {
            if (lbMediaChannels.SelectedIndex >= 0)
            {
                GCoreMediaChannelDescriptor mediaChannelDescriptor = (GCoreMediaChannelDescriptor)lbMediaChannels.SelectedItem;
                SetViewerMediaChannel(mediaChannelDescriptor);
            }
        }

        private void SetViewer(IntPtr hWnd, CustomDrawCallbackDelegate customDrawCallback)
        {
            if (_gngViewer != null)
            {
                _gngViewer.CloseCustomDrawCallBack();
                _gngViewer.Disconnect(true);
                _gngViewer.Dispose();
                _gngViewer = null;
            }

            //_gscViewer = new GscViewer(hWnd, true);
            _gngViewer = new GngViewer(hWnd, true);
            _gngViewer.SetCustomDrawCallBack(customDrawCallback);
        }

        private void SetViewerMediaChannel(GCoreMediaChannelDescriptor mediaChannelDescriptor)
        {
            if (_gngViewer == null || _gngServer == null)
            {
                return;
            }

            //_gscViewer.GetStatus(out GscViewerViewerStatus viewerStatus);
            _gngViewer.GetStatus(out GngViewerViewerStatus viewerStatus);

            if (viewerStatus.MediaChID != mediaChannelDescriptor.MediaChannelID)
            {
                //GscViewerConnectData connectData = new GscViewerConnectData
                GngViewerConnectData connectData = new GngViewerConnectData
                {
                    //GeViScopeServer = _gscServer,
                    GngServer = _gngServer,
                    MediaChID = mediaChannelDescriptor.MediaChannelID
                };
                //_gscViewer.ConnectDB(connectData, GscViewerPlayMode.pmPlayStream);
                _gngViewer.ConnectDB(connectData, GngViewerPlayMode.pmPlayStream);
            }
        }

        public GCoreForm(IntPtr appHandle)
        {
            InitializeComponent();

            SetClientSizeCore(640, 480);

            MediaPlayerHelperFunctions.InitializeMediaPlayerDLL(appHandle);

            SetViewer(pnlViewer.Handle, new CustomDrawCallbackDelegate(CustomDrawCallback));

            _gCoreViewerForm = new GCoreViewerForm();
        }

        public void CancelSnapshot()
        {
            _snapshotImage = null;
            _gCoreViewerForm.Hide();
            lbMediaChannels.Enabled = true;
        }

        public void SaveSnapshot(string filename)
        {
            if (_snapshotImage != null)
            {
                _snapshotImage.Save(filename);
            }
        }

        public void ShowMessage(string message)
        {
            _message = message;
        }

        public void ShowVideo()
        {
            _message = null;
        }

        public void TakeSnapshot(string comments)
        {
            Invoke((Action)(() =>
            {
                STATUS status = CaptureImage(out _snapshotImage, comments);

                Graphics g;
                switch (status)
                {
                    case STATUS.OK:
                        // Nothing to do if image is good
                        break;

                    case STATUS.NO_SERVER_CONNECTION:
                        _snapshotImage = new Bitmap(640, 480);
                        g = Graphics.FromImage(_snapshotImage);
                        g.FillRectangle(Brushes.Red, new Rectangle(0, 0, 640, 480));
                        g.DrawString("No Server Connection", new Font("Arial", 16), Brushes.White, 0, 0);
                        g.Flush();
                        break;

                    case STATUS.NO_VIDEO_CONNECTION:
                        _snapshotImage = new Bitmap(640, 480);
                        g = Graphics.FromImage(_snapshotImage);
                        g.FillRectangle(Brushes.Red, new Rectangle(0, 0, 640, 480));
                        g.DrawString("No Video Connection", new Font("Arial", 16), Brushes.White, 0, 0);
                        g.Flush();
                        break;

                    case STATUS.UNKNOWN:
                    default:
                        _snapshotImage = new Bitmap(640, 480);
                        g = Graphics.FromImage(_snapshotImage);
                        g.FillRectangle(Brushes.Red, new Rectangle(0, 0, 640, 480));
                        g.DrawString("Unknown Error", new Font("Arial", 16), Brushes.White, 0, 0);
                        g.Flush();
                        break;
                }

                lbMediaChannels.Enabled = false;
                _gCoreViewerForm.SetSnapshot(_snapshotImage);
                _gCoreViewerForm.Show();
            }));
        }

        private void SnapshotForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _snapshotImage = null;
            _gCoreViewerForm.Close();
            Application.Exit();
        }
    }
}
