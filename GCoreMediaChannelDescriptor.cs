﻿using System;

namespace GCoreLibrary
{
    public class GCoreMediaChannelDescriptor
    {
        private Int64 _mediaChannelId;
        private Int64 _globalNumber;
        private string _name;
        private string _description;
        private bool _blocked;

        public GCoreMediaChannelDescriptor(Int64 mediaChannelId, Int64 globalNumber, string name, string description, bool blocked)
        {
            _mediaChannelId = mediaChannelId;
            _globalNumber = globalNumber;
            _name = name;
            _description = description;
            _blocked = blocked;
        }

        public Int64 MediaChannelID
        {
            get => _mediaChannelId;
        }

        public string Name
        {
            get => _name;
        }

        public bool Blocked
        {
            get => _blocked;
        }
    }
}
