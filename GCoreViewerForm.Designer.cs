﻿namespace GCoreLibrary
{
    partial class GCoreViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbSnapshot = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbSnapshot)).BeginInit();
            this.SuspendLayout();
            // 
            // pbSnapshot
            // 
            this.pbSnapshot.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbSnapshot.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbSnapshot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSnapshot.Location = new System.Drawing.Point(12, 12);
            this.pbSnapshot.Name = "pbSnapshot";
            this.pbSnapshot.Size = new System.Drawing.Size(419, 310);
            this.pbSnapshot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSnapshot.TabIndex = 0;
            this.pbSnapshot.TabStop = false;
            // 
            // SnapshotViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 334);
            this.ControlBox = false;
            this.Controls.Add(this.pbSnapshot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SnapshotViewerForm";
            this.Text = "Snapshot Viewer";
            ((System.ComponentModel.ISupportInitialize)(this.pbSnapshot)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbSnapshot;
    }
}