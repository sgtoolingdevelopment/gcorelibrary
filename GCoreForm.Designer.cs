﻿namespace GCoreLibrary
{
    partial class GCoreForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

                if (_gngViewer != null)
                {
                    _gngViewer.CloseCustomDrawCallBack();
                    _gngViewer.Disconnect(true);
                    _gngViewer.Dispose();
                    _gngViewer = null;
                }

                if (_gngServer != null)
                {
                    _gngServer.Disconnect(System.Threading.Timeout.Infinite);
                    _gngServer.Dispose();
                    _gngServer = null;
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.lbMediaChannels = new System.Windows.Forms.ListBox();
            this.pnlViewer = new System.Windows.Forms.Panel();
            this.edtVideoServerAddress = new System.Windows.Forms.TextBox();
            this.labelVideoServer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(11, 57);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.OnBtnConnectClick);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.Location = new System.Drawing.Point(105, 57);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(75, 23);
            this.btnDisconnect.TabIndex = 2;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.OnBtnDisconnectClick);
            // 
            // lbMediaChannels
            // 
            this.lbMediaChannels.FormattingEnabled = true;
            this.lbMediaChannels.HorizontalScrollbar = true;
            this.lbMediaChannels.Location = new System.Drawing.Point(11, 86);
            this.lbMediaChannels.Name = "lbMediaChannels";
            this.lbMediaChannels.Size = new System.Drawing.Size(169, 199);
            this.lbMediaChannels.TabIndex = 3;
            this.lbMediaChannels.SelectedIndexChanged += new System.EventHandler(this.OnLbMediaChannelSelectedIndexChanged);
            // 
            // pnlViewer
            // 
            this.pnlViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlViewer.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlViewer.Location = new System.Drawing.Point(186, 12);
            this.pnlViewer.Name = "pnlViewer";
            this.pnlViewer.Size = new System.Drawing.Size(426, 418);
            this.pnlViewer.TabIndex = 4;
            // 
            // edtVideoServerAddress
            // 
            this.edtVideoServerAddress.Location = new System.Drawing.Point(12, 31);
            this.edtVideoServerAddress.Name = "edtVideoServerAddress";
            this.edtVideoServerAddress.Size = new System.Drawing.Size(168, 20);
            this.edtVideoServerAddress.TabIndex = 0;
            this.edtVideoServerAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnEdtVideoServerAddressKeyPress);
            // 
            // labelVideoServer
            // 
            this.labelVideoServer.AutoSize = true;
            this.labelVideoServer.Location = new System.Drawing.Point(12, 12);
            this.labelVideoServer.Name = "labelVideoServer";
            this.labelVideoServer.Size = new System.Drawing.Size(109, 13);
            this.labelVideoServer.TabIndex = 5;
            this.labelVideoServer.Text = "Video Server Address";
            // 
            // SnapshotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.labelVideoServer);
            this.Controls.Add(this.edtVideoServerAddress);
            this.Controls.Add(this.pnlViewer);
            this.Controls.Add(this.lbMediaChannels);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnConnect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SnapshotForm";
            this.Text = "Snapshot";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SnapshotForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.ListBox lbMediaChannels;
        private System.Windows.Forms.Panel pnlViewer;
        private System.Windows.Forms.TextBox edtVideoServerAddress;
        private System.Windows.Forms.Label labelVideoServer;
    }
}

